﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Database/Tools")]
public class ToolDatabase : ScriptableObject
{
    public static ToolDatabase Global { get { return ConfigRepository.Instance.Get<ToolDatabase>(); } }

    public ToolData[] Data;
}
