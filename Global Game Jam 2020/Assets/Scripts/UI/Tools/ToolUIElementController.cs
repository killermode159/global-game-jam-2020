﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolUIElementController : MonoBehaviour
{
    public ToolData ToolData => toolData;

    private ToolData toolData;
    public Image IconImage;
    private Animator animator;
    public ToolSelectorUIController ParentController { get; set; }

    public ToolUIElementController LeftController { get; set; }
    public ToolUIElementController RightController { get; set; }

    public Vector3 NextPosition { get; set; }

    Image bgImage;
    Transform parent;

    void Start()
    {
        bgImage = GetComponent<Image>();
        animator = GetComponent<Animator>();
        parent = transform.parent;
    }

    public void Initialize(ToolData data)
    {
        toolData = data;
        IconImage.overrideSprite = toolData.ToolSprite;
    }

    public void Move(float duration)
    {
        StartCoroutine(MoveTask(duration));
    }

    public IEnumerator MoveTask(float duration)
    {
        Vector3 startPos = transform.position;
        float currentTime = 0;
        float percentage = 0;

        while(percentage < 1){

            currentTime += Time.deltaTime;
            percentage = currentTime / duration;

            transform.parent.position = Vector3.Lerp(startPos, NextPosition, percentage);
            yield return new WaitForEndOfFrame();
        }
    }

    public void Select()
    {
        animator.SetInteger("State", 2);
        ParentController.SetSelected(this);
        transform.parent.SetAsLastSibling();
    }

    public void Hide()
    {
        IconImage.color = new Color(255, 255, 255, 30);
        bgImage.color = new Color(255, 255, 255, 30);
    }

    public void MoveDown(float duration)
    {
        animator.SetInteger("State", 3);
        Move(duration);
    }

    public void Finish(float duration)
    {
        animator.SetInteger("State", 4);
        Move(duration);
    }

    public void Deselect()
    {
        animator.SetInteger("State", 1);
    }

}
