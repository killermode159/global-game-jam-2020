﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolSelectorUIController : MonoBehaviour
{
    public ToolUIElementController Selected { get; private set; }

    public List<ToolUIElementController> controllers;
    [SerializeField] private GameObject prefab;
    [SerializeField] private ButtonSequenceHandler sequenceHandler;
	[SerializeField] private float moveDuration;

    private ToolDatabase database;
	private bool isUIMoving = false;
    private bool isRepairing = false;

    private Vector3 selectedPosition;
    public ItemUIElementController Item;
    public GameEvent EvtDamagePlayer;

    private void Start()
    {
        database = ToolDatabase.Global;
        Selected = controllers[1];
        selectedPosition = Selected.transform.position;
        for (int i = 0; i < controllers.Count; i++)
        {
            controllers[i].ParentController = this;

            controllers[i].Initialize(database.Data[i]);

            controllers[i].LeftController = controllers[GetIndexToThe(i, -1)];
            controllers[i].RightController = controllers[GetIndexToThe(i, 1)];
        }

		Item.Show();
    }

    public void SetSelected(ToolUIElementController child)
    {
        Selected = child;
    }

    /// <summary>
    /// Used to get indexes of tools in list to reference.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="position"></param>
    /// <returns></returns>
    int GetIndexToThe(int value, int position)
    {
        int index = value + position;
        if (index >= controllers.Count) return 0;
        else if (index < 0) return controllers.Count - 1;
        else return value + position;
    }

    public void MoveLeft()
    {
		if (isUIMoving || isRepairing)
			return;

		StartCoroutine(MoveTimerTask());

        Selected.Deselect();
        Selected.RightController.Select();
        
        foreach(ToolUIElementController controller in controllers)
        {
            controller.NextPosition = controller.LeftController.transform.position;
        }

        foreach (ToolUIElementController controller in controllers)
        {
			controller.Move(moveDuration);
        }
    }

    public void MoveRight()
    {
		if (isUIMoving || isRepairing)
			return;

		StartCoroutine(MoveTimerTask());

		foreach (ToolUIElementController controller in controllers)
        {
            controller.NextPosition = controller.RightController.transform.position;
        }

        Selected.Deselect();
        Selected.LeftController.Select();

        foreach (ToolUIElementController controller in controllers)
        {
            controller.Move(moveDuration);
        }
    }

    public  void MoveDown()
    {
        if (isRepairing) return;

        isRepairing = true;
        Selected.NextPosition = Item.transform.position;
        Selected.MoveDown(moveDuration);

        StartCoroutine(MoveTimerTask());
        Repair();
    }

    public void Repair()
    {
        StartCoroutine(RepairTask());
    }

    public IEnumerator RepairTask()
    {
        yield return new WaitUntil(() => !isUIMoving);
		if (Item.Data.IsFixed())
		{
			yield break;
		}
		else if (Selected.ToolData.IsDamageToRepair(Item.Data.CheckDamageType()))
		{
			Item.StartRepairSequence();
		}
		else
		{
			EvtDamagePlayer.Raise();
			MoveUp();
		}
    }

    public void MoveUp()
    {
        if (sequenceHandler.HasSequenceStarted) return;

        isRepairing = false;

        Selected.NextPosition = selectedPosition;
        Selected.Finish(moveDuration);

        StartCoroutine(MoveTimerTask());
    }

	private IEnumerator MoveTimerTask()
	{
		isUIMoving = true;
		yield return new WaitForSeconds(moveDuration);
		isUIMoving = false;
	}

#warning temp
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            MoveLeft();
        }
        else if (Input.GetKeyDown(KeyCode.B))
        {
            MoveRight();
        }
    }

}
