﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ConfigRepository : MonoBehaviour
{
    private static ConfigRepository instance;
    public static ConfigRepository Instance
    {
        get => instance;
        
    }

    private void Awake()
    {
        instance = this;
    }

    public Object[] Configs;

    public T Get<T>()
        where T : Object
    {
        T database = instance.Configs.Where(d => d is T).SingleOrDefault() as T;
        return database;
    }
}
