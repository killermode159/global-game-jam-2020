﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemUIElementController : MonoBehaviour
{
    private Image Icon;
    
    public ItemData Data { get; private set; }

    private ItemDatabase database;
	private Transform parentTransform;
    private Animator animator;

    public GameEvent EvtRepairStarted;

    // Start is called before the first frame update
    void Awake()
    {
        Icon = GetComponent<Image>();
        database = ItemDatabase.Global;
        animator = GetComponent<Animator>();

    }

    public void Show()
    {
        Data = database.GetRandomItem();

        Icon.overrideSprite = Data.SpriteStates.ItemStateSprite(Data.CheckDamageType().DamageState);
        animator.SetInteger("State", 1);
    }

    public void Fix()
    {
        Data.FixCurrentDamage();
		// If item is still damaged
		if (!Data.IsFixed())
		{
			Icon.overrideSprite = Data.SpriteStates.ItemStateSprite(Data.CheckDamageType().DamageState);
		}
		// else stamp of approval
		else
		{
			Icon.overrideSprite = Data.SpriteStates.ItemStateSprite(ItemState.Fixed);
			Complete();
		}
    }

    public void StartRepairSequence()
    {
        EvtRepairStarted.Raise();
    }

    public void Complete()
    {
        animator.SetInteger("State", 2);
    }

    public void Break()
    {
        animator.SetInteger("State", 3);
    }

    IEnumerator RepairTasks()
    {
        yield return null;
    }



}
