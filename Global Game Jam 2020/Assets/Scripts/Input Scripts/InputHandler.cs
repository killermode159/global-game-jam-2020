﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class InputHandler : MonoBehaviour
{
	private static InputHandler instance;
	public static InputHandler Instance => instance;

	[SerializeField] private ButtonMapSettings buttonMapping;

	int xDir = 0;
	int yDir = 0;

	bool hasXInput = false;
	bool hasYInput = false;

	private void Awake()
	{
		Assert.IsNull(instance, "InputHandler already exists in the scene");
		instance = this;
	}

	private void Update()
    {
		

		float horizontal = Mathf.RoundToInt(Input.GetAxis(buttonMapping.AxisX.AxisName));
		float vertical = Mathf.RoundToInt(Input.GetAxis(buttonMapping.AxisY.AxisName));

		if (horizontal == 0 && hasXInput)
		{
			hasXInput = false;
			xDir = 0;
		}
		else if(horizontal != 0 && !hasXInput)
		{
			xDir = (int)horizontal;

			#region For Events (on hold)
			//if (xDir == -1)
			//{
			//	buttonMapping.AxisX.AxisEventNegative.Raise();
			//	//Debug.Log("Joystick Left");
			//}
			//else if (xDir == 1)
			//{
			//	buttonMapping.AxisX.AxisEventPositive.Raise();
			//	Debug.Log("Joystick Right");
			//}
			#endregion
		}

		if (vertical == 0 && hasYInput)
		{
			hasYInput = false;
			yDir = 0;
		}
		else if (vertical != 0 && !hasYInput)
		{
			yDir = (int)vertical;

			#region For Events (on hold)
			//if (yDir == -1)
			//{
			//	buttonMapping.AxisY.AxisEventNegative.Raise();
			//	Debug.Log("Joystick Down");
			//}
			//else if (yDir == 1)
			//{
			//	buttonMapping.AxisY.AxisEventPositive.Raise();
			//	Debug.Log("Joystick Up");
			//}
			#endregion
		}

		OnAxisLeft();
		OnAxisRight();
		OnAxisUp();
		OnAxisDown();

		#region For Debugging
		//for (int i = 0; i < buttonMapping.ButtonMap.Count; i++)
		//{
		//	if (Input.GetKeyDown(buttonMapping.ButtonMap[i].Button))
		//	{
		//		Debug.Log(buttonMapping.ButtonMap[i].ButtonName);
		//	}
		//}

		//if (OnAxisRight())
		//	Debug.Log("Right");
		//if (OnAxisLeft())
		//	Debug.Log("Left");
		//if (OnAxisUp())
		//	Debug.Log("Up");
		//if (OnAxisDown())
		//	Debug.Log("Down");

		#endregion
	}

	public bool OnAxisLeft()
	{
		if (hasXInput || xDir >= 0)
		{
			return false;
		}
		hasXInput = true;
		buttonMapping.AxisX.AxisEventNegative.Raise();
		return xDir == -1;
	}
	public bool OnAxisRight()
	{
		if (hasXInput || xDir <= 0)
		{
			return false;
		}
		hasXInput = true;

		buttonMapping.AxisX.AxisEventPositive.Raise();
		return xDir == 1;
	}
	public bool OnAxisUp()
	{ 
		if (hasYInput || yDir <= 0)
		{
			return false;
		}
		hasYInput = true;
		buttonMapping.AxisY.AxisEventPositive.Raise();
		return yDir == 1;
	}
	public bool OnAxisDown()
	{
		if (hasYInput || yDir >= 0)
		{
			return false;
		}
		hasYInput = true;
		buttonMapping.AxisY.AxisEventNegative.Raise();
		return yDir == -1;
	}

	public bool OnButtonDown(string buttonName)
	{
		InputButton button = buttonMapping.Find(buttonName);
		Debug.Log(button.ButtonName);
		//if (button == null)
		//	return false;
		button.ButtonEvent?.Raise();
		return Input.GetKeyDown(button.Button);
	}

	public bool OnAnyButtonDown()
	{
		foreach (InputButton inputButton in buttonMapping.ButtonMap)
		{
			if(Input.GetKeyDown(inputButton.Button))
			{
				Debug.Log("Pressed [" + inputButton.ButtonName + "]");
 				return true;
			}
		}
		return false;
	}

	public bool OnAnyButtonDownExcept(string exception)
	{
		foreach (InputButton inputButton in buttonMapping.ButtonMap)
		{
			if (inputButton.ButtonName == exception)
				continue;

			if (Input.GetKeyDown(inputButton.Button))
			{
				Debug.Log("Pressed [" + inputButton.ButtonName + "]");
				return true;
			}
		}
		return false;
	}
}
