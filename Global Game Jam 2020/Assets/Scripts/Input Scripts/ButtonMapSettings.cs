﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class InputButton
{
	public string ButtonName => buttonName;
	public KeyCode Button => inputButton;
	public GameEvent ButtonEvent => buttonEvent;

	[SerializeField] private string buttonName;
	[SerializeField] private KeyCode inputButton;
	[SerializeField] private GameEvent buttonEvent;
}

[Serializable]
public class InputAxis
{
	public string AxisName => axisName;
	public GameEvent AxisEventNegative => axisEventNegative;
	public GameEvent AxisEventPositive => axisEventPositive;

	[SerializeField] private string axisName;
	[SerializeField] private GameEvent axisEventNegative;
	[SerializeField] private GameEvent axisEventPositive;
}

[CreateAssetMenu(menuName = "Input/Button Mapping", fileName = "New Button Mapping")]
public class ButtonMapSettings : ScriptableObject
{
	public List<InputButton> ButtonMap => inputButtons;
	public InputAxis AxisX => joystickX;
	public InputAxis AxisY => joystickY;

	[SerializeField] private List<InputButton> inputButtons;
	[SerializeField] private InputAxis joystickX;
	[SerializeField] private InputAxis joystickY;

	public InputButton Find(string buttonName)
	{
		//Debug.Log("Paramater [" + buttonName + "] | Button ")
		return inputButtons.Find(x => string.Equals(x.ButtonName, buttonName));//, StringComparison.CurrentCultureIgnoreCase));

	}
}
