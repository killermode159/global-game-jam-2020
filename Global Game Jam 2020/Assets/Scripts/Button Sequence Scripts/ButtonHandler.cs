﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHandler : MonoBehaviour
{
	public int Id => buttonId;
	public string InputButton => inputButton;

	[SerializeField] private int buttonId = 0;
	[SerializeField] private string inputButton;

	private Image image;
	private Color originalColor;

	private void Awake()
	{
		image = GetComponent<Image>();
		originalColor = image.color;
	}

	public void Glow()
	{
		image.color = Color.blue;
	}

	public void Unglow()
	{
		image.color = originalColor;
	}

	public void SuccessfulHit()
	{
		StartCoroutine(StaggeredGlowTask());
	}

	private IEnumerator StaggeredGlowTask()
	{
		image.color = Color.green;
		yield return new WaitForSeconds(0.2f);
		Unglow();
	}
}