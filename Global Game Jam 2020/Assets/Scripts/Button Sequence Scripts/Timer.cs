﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Timer
{
	public enum CoroutineState
	{
		Started,
		Ongoing,
		Finished
	}

	private static Coroutine timerCoroutine;
	private static CoroutineState timerState;

	private delegate void OnCoroutineStateUpdate(float currentTime);
	private static OnCoroutineStateUpdate onTimerStart;
	private static OnCoroutineStateUpdate onTimerUpdate;
	private static OnCoroutineStateUpdate onTimerFinished;

	public static void StartTimer(MonoBehaviour mono, float duration)
	{
		timerState = CoroutineState.Started;
		onTimerStart.Invoke(0);
		
		timerCoroutine = mono.StartCoroutine(TimerTask(duration));
	}

	public static void StopTimer(MonoBehaviour mono)
	{
		timerState = CoroutineState.Finished;
		onTimerFinished.Invoke(0);
		if (timerCoroutine == null)
			return;
		mono.StopCoroutine(timerCoroutine);
	}

	private static IEnumerator TimerTask(float duration)
	{
		float currentTime = 0;
		float perc = 0;

		timerState = CoroutineState.Ongoing;

		while (perc < 1)
		{
			currentTime += Time.deltaTime;
			perc = currentTime / duration;

			onTimerUpdate.Invoke(currentTime);

			yield return new WaitForEndOfFrame();
		}

		timerState = CoroutineState.Finished;
		onTimerFinished.Invoke(duration);
	}
}