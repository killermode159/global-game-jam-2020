﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ButtonSequenceHandler : MonoBehaviour
{
    public bool HasSequenceStarted => hasSequenceStarted;

	[SerializeField] private GameEvent onButtonSequenceStart = null;
	[SerializeField] private GameEvent onButtonSequenceEnd = null;
	[SerializeField] private GameEvent onButtonSequenceWin = null;
	[SerializeField] private GameEvent onButtonSequenceLose = null;
	[SerializeField] private GameEvent onButtonPress = null;
 
	[SerializeField] private List<ButtonHandler> buttonHandlers = new List<ButtonHandler>();

	[SerializeField] private bool hasSequenceStarted = false;
	[SerializeField] private float buttonWindowDuration = 1;
	[SerializeField] private float buttonDownTime = 1;

	//NOTE: temporary implementation of difficulty
	[SerializeField] private int difficulty = 12;

	private Queue<int> buttonSequenceQueue = new Queue<int>();
	// if the player should hit the button
	private bool shouldHit = false;
	// if the player can still hit any button (prevents multiple button presses)
	private bool canHit = false;

	private ButtonHandler correctButton = null;

	// Start is called before the first frame update
	void Start()
	{
		buttonHandlers.OrderBy(b => b.Id);
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.O) || InputHandler.Instance.OnButtonDown("Menu"))
		{
			Debug.Log("TEST");
			StartButtonSequence();
		}
		if (Input.GetKeyDown(KeyCode.P))
		{
			EndButtonSequence();
		}
		
		if (!hasSequenceStarted)
			return;

		// If the player should hit the button and has not pressed any other button
		if (shouldHit && canHit)
		{
			// If the player hit the right input button
			if (InputHandler.Instance.OnButtonDown(correctButton.InputButton))
			{
				correctButton.SuccessfulHit();
				canHit = false;
				onButtonPress.Raise();
				StopCoroutine(ButtonWindowTimerTask(correctButton));
			}
			// If the player presses any other button
			else if(InputHandler.Instance.OnAnyButtonDownExcept("Menu"))
			{
				canHit = false;
				Lose();
			}
		}
		else if (!shouldHit)
		{
			if (InputHandler.Instance.OnAnyButtonDownExcept("Menu"))
			{
				Lose();
			}
		}
	}

	public void StartButtonSequence()
	{
		Debug.Log("TEST1");
		if (hasSequenceStarted)
		{
			Debug.Log("TEST2");
			return;
		}
		Debug.Log("TEST3");
		hasSequenceStarted = true;
		onButtonSequenceStart.Raise();

		GenerateRandomSequence();
		StartCoroutine(ButtonSequenceTask());
	}

	public void EndButtonSequence()
	{
		if (!hasSequenceStarted)
		{
			return;
		}

		hasSequenceStarted = false;

		canHit = false;
		shouldHit = false;

		correctButton.Unglow();

		ClearSequence();
		StopAllCoroutines();
		onButtonSequenceEnd.Raise();

	}

	private void Win()
	{
		EndButtonSequence();
		onButtonSequenceWin.Raise();
	}

	private void Lose()
	{
		EndButtonSequence();
		onButtonSequenceLose.Raise();
	}

	private void ClearSequence()
	{
		float count = buttonSequenceQueue.Count;
		for (int i = 0; i < count; i++)
		{
			buttonSequenceQueue.Dequeue();
		}
	}

	private void GenerateRandomSequence()
	{
		for (int i = 0; i < difficulty; i++)
		{
			int randomButtonId = Random.Range(0, buttonHandlers.Count);
			buttonSequenceQueue.Enqueue(randomButtonId);
		}
	}

	private IEnumerator ButtonSequenceTask()
	{
		int activeButton = -1;

		// while the the button sequence queue is not empty
		while (buttonSequenceQueue.Count > 0)
		{
			// Get the next button id to press from the queue
			activeButton = buttonSequenceQueue.Dequeue();
			// Then get the corresponding button from the list of button handlers
			correctButton = buttonHandlers.Find(x => x.Id == activeButton);
			
			// Wait until the button down time is finished
			yield return StartCoroutine(ButtonDownTimerTask());
			yield return null;

			// Wait until the player presses the correct button or misses it
			yield return StartCoroutine(ButtonWindowTimerTask(correctButton));
			yield return null;
		}
		// If the player should've hit a button and the queue is empty, then they lose
		if (canHit)
		{
			// Lose
			Lose();
			yield break;
		}

		Win();
	}

	private IEnumerator ButtonDownTimerTask()
	{
		// If the player can still hit a button during the button down time, they lose
		if (canHit)
		{
			// Lose
			EndButtonSequence();
			onButtonSequenceLose.Raise();
			yield break;
		}

		float currentTime = 0;
		float perc = 0;

		while (perc < 1)
		{
			currentTime += Time.deltaTime;
			perc = currentTime / buttonDownTime;

			shouldHit = false;
			

			yield return new WaitForEndOfFrame();
		}
	}

	private IEnumerator ButtonWindowTimerTask(ButtonHandler correctButton)
	{

		shouldHit = true;
		canHit = true;
		correctButton.Glow();

		float currentTime = 0;
		float perc = 0;

		while (perc < 1)
		{
			currentTime += Time.deltaTime;
			perc = currentTime / buttonWindowDuration;
			

			yield return new WaitForEndOfFrame();
		}
		correctButton.Unglow();
	}


}
