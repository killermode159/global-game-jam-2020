﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthIconHandler : MonoBehaviour
{
	[SerializeField] private Animation animation;
	private bool isAnimating;

	public void Remove()
	{
		if (isAnimating)
			return;
		isAnimating = true;
		animation.Play("heart-disappear");
	}

	private void Update()
	{
		if (isAnimating && !animation.isPlaying)
		{
			gameObject.SetActive(false);
		}
	}



}