﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUIHandler : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private Transform startPoint;
	[SerializeField] private HealthHandler healthHandler;
	[SerializeField] private GameObject healthUIPrefab;

	[Header("UI Properties")]
	[Space(10)]
	[SerializeField] private float distanceBetweenHealth = 5;

	[Header("Debug Properties")]
	[Space(10)]
	[SerializeField] private float debugSize = 0.25f;

	private List<GameObject> heartIcons = new List<GameObject>();

	private void Start()
	{
		ResetHealthUI();
	}

	private void HealthUIUpdate()
	{
		int healthDiff = healthHandler.MaxHealth - healthHandler.CurrentHealth;
		for(int i = 0; i < healthDiff; i++)
		{
			if (!heartIcons[heartIcons.Count - 1 - i].activeSelf)
				continue;

			heartIcons[heartIcons.Count - 1 - i].GetComponent<HealthIconHandler>().Remove();
			Debug.Log("Test");
		}
	}

	public void ResetHealthUI()
	{
		for (int i = heartIcons.Count - 1; i >= 0; i--)
		{
			Destroy(heartIcons[i]);
		}
		heartIcons.Clear();

		for (int i = 0; i < healthHandler.MaxHealth; i++)
		{
			GameObject heart = Instantiate(healthUIPrefab, transform);
			heart.transform.position = startPoint.position + (Vector3.right * (i * distanceBetweenHealth));
			heartIcons.Add(heart);
		}
	}

	private void OnDrawGizmos()
	{
		if (!startPoint || !healthHandler)
			return;

		for (int i = 0; i < healthHandler.MaxHealth; i++)
		{
			Gizmos.DrawWireSphere(startPoint.position + (Vector3.right * (i * distanceBetweenHealth)), debugSize);
		}

	}
}