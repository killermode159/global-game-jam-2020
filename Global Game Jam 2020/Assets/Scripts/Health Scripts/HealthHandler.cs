﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthHandler : MonoBehaviour
{
	public int MaxHealth => maxHealth;
	public int CurrentHealth => currentHealth;

	[SerializeField] private int maxHealth;
	[SerializeField] private GameEvent onHealthLoss;
	[SerializeField] private GameEvent onHealthReset;

	private int currentHealth;

	private void Start()
	{
		ResetHealth();
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			ReduceHealth();
		}
		if (Input.GetKeyDown(KeyCode.F))
		{
			ResetHealth();
		}
	}

	public void ReduceHealth()
	{
		currentHealth--;
		if (currentHealth < 0)
			currentHealth = 0;
		onHealthLoss.Raise();
	}

	public void ResetHealth()
	{
		currentHealth = maxHealth;
		onHealthReset.Raise();
	}
}
