﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{
	public UnityEvent OnEventRaise => onEventRaise;
 
	[SerializeField] private GameEvent gameEvent;
	[SerializeField] private UnityEvent onEventRaise;

	private void OnEnable()
	{
		if (!gameEvent)
		{
			Debug.LogWarning("Game Event is null", this);
			return;
		}
		gameEvent.RegisterListener(this);

	}

	private void OnDisable()
	{
		if (!gameEvent)
		{
			Debug.LogWarning("Game Event is null", this);
			return;
		}
		gameEvent.UnregisterListener(this);
	}
}
