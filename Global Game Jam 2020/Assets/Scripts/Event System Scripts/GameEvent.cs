﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Event")]
public class GameEvent : ScriptableObject
{
    private List<GameEventListener> listeners = new List<GameEventListener>();

	public void Raise()
	{
		for (int i = 0; i < listeners.Count; i++)
		{
			listeners[i].OnEventRaise.Invoke();
		}
	}

	public void RegisterListener(GameEventListener listener)
	{
		listeners.Add(listener);
	}

	public void UnregisterListener(GameEventListener listener)
	{
		listeners.Remove(listener);
	}

    
}