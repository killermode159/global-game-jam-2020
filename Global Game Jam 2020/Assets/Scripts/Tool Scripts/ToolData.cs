﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tool/Tool Data")]
public class ToolData : ScriptableObject
{
    public Sprite ToolSprite => toolSprite;
    public DamageTypeData DamageType => type;
    public string Name => toolName;

    [SerializeField] private string toolName;
    [SerializeField] private DamageTypeData type;
    [SerializeField] private Sprite toolSprite;

    public bool IsDamageToRepair(DamageTypeData damage)
    {
		Debug.Log(damage.DamageState);
        return type == damage;
    }
}
