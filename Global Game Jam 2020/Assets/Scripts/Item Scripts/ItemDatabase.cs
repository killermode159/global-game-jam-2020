﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Database/Items")]
public class ItemDatabase : ScriptableObject
{
	public static ItemDatabase Global { get { return ConfigRepository.Instance.Get<ItemDatabase>(); } }

	[SerializeField] ItemData[] Data;
	[SerializeField] int MaxDamageType;

	public ItemData GetRandomItem()
	{
		int itemRoll = Random.Range(0, Data.Length);

		int damageRoll = Random.Range(1, MaxDamageType);
		
		ItemData data = Data[itemRoll];	
		data.ClearDamage();

		DamageTypeDatabase database = DamageTypeDatabase.Global;
		DamageTypeData randomDamageType = null;

		for (int i = 0; i < damageRoll; i++)
		{
			do
			{
				randomDamageType = database.GetRandomDamage();
			} while (data.HasDamage(randomDamageType));

			data.AddDamage(database.GetRandomDamage());
		}

		return data;
	}
}
