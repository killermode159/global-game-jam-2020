﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemSprite
{
    public Sprite FixedState => fixedState;
    public Sprite SplitState => splitState;
    public Sprite ShatteredState => shatteredState;
    public Sprite CurseState => cursedState;

    [SerializeField] private Sprite fixedState;
    [SerializeField] private Sprite splitState;
    [SerializeField] private Sprite shatteredState;
    [SerializeField] private Sprite cursedState;

    public Sprite ItemStateSprite(ItemState state)
    {
        switch (state)
        {
            case ItemState.Split:
                return splitState;

            case ItemState.Shattered:
                return shatteredState;

            case ItemState.Cursed:
                return cursedState;

            default:
                return fixedState;
        }
    }
}

public enum ItemState
{
    Split,
    Shattered,
    Cursed,
    Fixed
}

[CreateAssetMenu(menuName = "Item/Item Data")]
public class ItemData : ScriptableObject
{
    public string Name => itemName;
    public ItemSprite SpriteStates => spriteStates;

    [SerializeField] private string itemName;
    [SerializeField] private ItemSprite spriteStates;

    private Queue<DamageTypeData> damage = new Queue<DamageTypeData>();

	// Adds a new damage type to the item
	public void AddDamage(DamageTypeData damageType) => damage.Enqueue(damageType);
	// Checks the current damage type to fix
	public DamageTypeData CheckDamageType() => damage.Peek();
	// Fixes the current damage and removes it from the damage queue
	public void FixCurrentDamage() => damage.Dequeue();

	// Checks if the item has a certain damage type
	public bool HasDamage(DamageTypeData damageType) => damage.Contains(damageType);
	// Checks if the item has no more damage
	public bool IsFixed() => DamageCount() <= 0;
	// Checks the number of damage left
	public int DamageCount() => damage.Count;

	public void ClearDamage() => damage.Clear();

}
