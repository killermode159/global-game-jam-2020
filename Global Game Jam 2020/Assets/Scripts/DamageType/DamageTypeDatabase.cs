﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Database/Damage")]
public class DamageTypeDatabase : ScriptableObject
{
    public static DamageTypeDatabase Global { get { return ConfigRepository.Instance.Get<DamageTypeDatabase>(); } }

    [SerializeField] DamageTypeData[] Data;

    public DamageTypeData GetRandomDamage()
    {
        return Data[Random.Range(0, Data.Length)];
    }
}
