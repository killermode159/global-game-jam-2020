﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tool/Damage Type")]
public class DamageTypeData : ScriptableObject
{
    public ItemState DamageState => damageState;

    [SerializeField] private string name;
    [SerializeField] private ItemState damageState;
}
