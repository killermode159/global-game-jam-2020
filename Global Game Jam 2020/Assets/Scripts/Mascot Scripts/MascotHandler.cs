﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MascotHandler : MonoBehaviour
{
	[SerializeField] private Animator mascotAnimator;

    public void PlayAnimation(string triggerName)
	{
		mascotAnimator.SetTrigger(triggerName);
		StartCoroutine(DelayIdleTask());
	}

	private IEnumerator DelayIdleTask()
	{
		yield return new WaitForSeconds(2);
		mascotAnimator.SetTrigger("Idle");
	}
}
