﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Countdown : MonoBehaviour
{
    public int TimeWindow { get; set; } 
    public bool WindowOpen { get; private set; }

    private float timer;

    IEnumerator TimerTask()
    {

        while(WindowOpen)
        {

            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        
    }
}
